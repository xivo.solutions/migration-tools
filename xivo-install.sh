#!/bin/bash
set -e

mirror_xivo="http://mirror.xivo.solutions"
update='apt-get update'
install='apt-get install --assume-yes'
download='apt-get install --assume-yes --download-only'


check_system() {
    local version_file='/etc/debian_version'
    if [ ! -f $version_file ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_name\") system"
        echo "You do not seem to be on a Debian system"
        exit 1
    else
        version=$(cut -d '.' -f 1 "$version_file")
    fi

    if [ $version != $debian_version ]; then
        echo "You must install XiVO on a Debian $debian_version (\"$debian_name\") system"
        echo "You are currently on a Debian $version system"
        exit 1
    fi
}

add_xivo_key() {
    wget $mirror_xivo/xivo_current.key -O - | apt-key add -
}

add_mirror() {
    echo "Add mirrors informations"
    local mirror="deb $mirror_xivo/$repo $distribution main"
    apt_dir="/etc/apt"
    sources_list_dir="$apt_dir/sources.list.d"
    if ! grep -qr "$mirror" "$apt_dir"; then
        echo "$mirror" > $sources_list_dir/tmp-pf.sources.list
    fi
    add_xivo_key

    export DEBIAN_FRONTEND=noninteractive
    $update
    $install xivo-dist
    xivo-dist "$distribution"

    rm -f "$sources_list_dir/tmp-pf.sources.list"
    $update
}

install_xivo () {
    wget -q -O - $mirror_xivo/d-i/$debian_name/pkg.cfg | debconf-set-selections

    kernel_release=$(uname -r)
    $install --purge postfix
    $download dahdi-linux-modules-$kernel_release xivo
    $install dahdi-linux-modules-$kernel_release
    $install xivo

    xivo-service restart all

    if [ $? -eq 0 ]; then
        echo 'You must now finish the installation'
        xivo_ip=$(ip a s eth0 | grep -E 'inet.*eth0' | awk '{print $2}' | cut -d '/' -f 1 )
        echo "open http://$xivo_ip to configure XiVO"
    fi
}

usage() {
    cat << EOF
    This script is used to install XiVO

    usage : $(basename $0) {-d|-r|-a version}
        whitout arg : install production version
        -r          : install release candidate version
        -d          : install development version
        -a version  : install archived version (14.18 or later)

EOF
    exit 1
}

distribution='xivo-five'
repo='debian'
debian_name='jessie'
debian_version='8'

while getopts ':dra:' opt; do
    case ${opt} in
        d)
            distribution='xivo-dev'
            ;;
        r)
            distribution='xivo-rc'
            ;;
        a)
            distribution="xivo-$OPTARG"
            repo='archive'

            if [ "$OPTARG" \> "15.19" ]; then
                debian_version='8'
                debian_name='jessie'
            elif [ "$OPTARG" \> "14.17" ]; then
                debian_version='7'
                debian_name='wheezy'
            else
                # 14.17 and earlier don't have xivo-dist available
                echo "This script only supports installing XiVO 14.18 or later."
                exit 1
            fi
            ;;
        *)
            usage
            ;;
    esac
done

check_system
add_mirror
install_xivo